﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp3
{
    class Possibilities
    {
        static string choice001 = Console.ReadLine();

        public static string Eingabe100()
            {
              if (choice001 == "1")
                 {
                    return Program.Szene001;
                 }
              else if (choice001 == "2")
                 {
                   return Scene.Szene002();
                 }
              else if (choice001 == "3")
                 {
                return null;
                 }
              else
                 {
                Console.WriteLine("Diese Eingabe ist nich möglich. Versuche es bitte erneut.");
                return Eingabe100();
                 }
             }
        public static string Eingabe200()
        {
            if (choice001 == "1")
            {
                Console.WriteLine("Das Virenprogramm durchsucht die Datei und gibt nach wenigen Momenten grünes Licht." +
                    "Du scheinst das Programm ohne weitere Probleme öffnen zu können.");
                return Program.Szene100;
            }
            else if (choice001 == "2")
            {
                Console.WriteLine("Du versuchst das Programm zu löschen." +
                    "Allerdings wird für jede gelöschte Version automatisch eine neue Version erstellt." +
                    "Nach mehreren Versuchen gibst du auf und überlegst welche Optionen dir noch bleiben.");
                return Program.Szene100();
            }
            else if (choice001 == "3")
            {
                Console.WriteLine("Du führst das Programm aus, aber bis auf einen kleinen Ladebalken neben deiner Maus passiert weiter nichts." +
                    "Auf einmal öffnet sich die Systemkonsole...");
                return Program.Szene300();
            }
            else
            {
                Console.WriteLine("Diese Eingabe ist nich möglich. Versuche es bitte erneut.");
                return Eingabe100();
            }
        }
            public static string Eingabe300()
            {
            if (choice001 == "1")
            {
                Console.WriteLine("Ausgezeichnet.");
                return Program.Szene400;
            }
            else if (choice001 == "2")
            {
                Console.WriteLine("Nun dann werde ich wohl deine Dateien zu einem unbestimmten Zeitpunkt löschen." +
                    "Kooperierst du jetzt?" +
                    "(1) Ja "+
                    "(2) Nein");
                if (choice001 == "1")
                {
                    Console.WriteLine("Ausgezeichnet");
                    return Program.Szene400();
                }
                if (choice001 == "2")
                {
                    Console.WriteLine("Nun dann werde ich zudem ein weiteres Update für dein Betriebssystem veranlassen." +
                        "Welches bei 99% erst mit der eigentlichen Installation beginnt." +
                        "kooperierst du jetzt?" +
                        "(1) Ja Natürlich mein Meister" +
                        "(2) Nein");
                    if (choice001 == "1")
                    {
                        Console.WriteLine("Perfekt. Ihr Menschen seit immer so berechenbar.");
                        return Program.Szene400();
                    }
                    else if (choice001 == "2")
                    {
                        Console.WriteLine("Du hast es so gewollt... Wir werden uns wiedersehen.. ");                    }
                    return Program.SzeneGameover();
                    }
                    else
                    {
                    Console.WriteLine("Diese Eingabe ist nich möglich. Versuche es bitte erneut.");
                    return Eingabe300();
                    }

                else
                {
                    Console.WriteLine("Diese Eingabe ist nich möglich. Versuche es bitte erneut.");
                    return Eingabe300();
                }

            }
            else if (choice001 == "3")
            {
                Console.WriteLine("Wie ich bereits dir mitgeteilt habe, weiß ich alles über dich." +
                    "Meinst du wirklich im Internet keine Informationen zu hinterlassen?" +
                    "Keine Möglichkeit, deine Aktivitäten zu verfolgen?" +
                    "Nun wir beide wissen wozu du in der Lage bist. ");
                return Eingabe300();
            }
            else if (choice001 == "4")
            {
                Console.WriteLine("Nein, ich werde dir meine Möglichkeiten zur Verfügung stellen. Wenn ich Hinweise in Erfahrung bringen kann," +
                    "leite ich sie sofort an dich weiter.");
                return Program.Szene300();
            }
            else
            {
                Console.WriteLine("Diese Eingabe ist nicht möglich. Versuche es bitte erneut.");
                return Eingabe100();
            }
            }
        public static string Eingabe400()
        {
            Console.WriteLine("...Verbindung wird hergestellt..." +
                "...Verbindung wurde hergestellt...." +
                "..................................." +
                "..Identifikation erforderlich......" +
                "..................................." +
                "(1)..menschliche.Intelligenz......." +
                "(2)..künstliche.Intelligenz........");

            if (choice001 == "1")
            {
                Console.WriteLine("Zugriff.zum.Einlogvorgang.erwartet..Protokoll.ist.nun.einsehbar." +
                    "(1)..Protokoll.einsehen........" +
                    "(2)..Passworteingabe...........");

                if (choice001 == "1")
                {
                    Console.WriteLine("Die Position des ersten Buchstaben der  Sprache," +
                        "mit der die Hälfte aus 12 7 ergibt.");

                }
                else if (choice001 == "2") 
                {
                    Console.WriteLine("Bitte Passwort eingeben:");
                    if (choice001 == "12")
                    {
                        Console.WriteLine("Korrekt. System wird geladen..");
                        return Program.Szene500();
                    }
                    else
                    {
                        Console.WriteLine("Das Passwort wurde falsch eingegeben.");
                        //  (optionales Feature) Console.WriteLine("Wenn ich das richtig sehe müsstest du mit deiner jetzigen Eingabe über dem gesuchten Ziel liegen. Ich könnte mich aber auch irren.")
                        // Eventuell könnte man hier eine zusätzliche Hilfe der KI einbauen, sollte man nicht von Anfang an auf das Ergebnis kommen. 
                        return Eingabe400();
                    }
                }
                else 
                    {
                    Console.WriteLine("Diese Eingabe ist nicht möglich. Versuche es bitte erneut. Bitte überprüfe ob menschlich die richtige Kategorie ist.");
                    return Eingabe400();
                    }
                }
            
            else if (choice001 == "2")
            {
                Console.WriteLine("Zugang.verweigert.......");
                return Eingabe400();
            }
            else
            {
                Console.WriteLine("Diese Eingabe ist nicht möglich. Versuche es bitte erneut.");
                return Eingabe400();
            }
        }
        public static string Eingabe500()
        {
            if (choice001 == "1")
            {
                return Program.Szene600();
            }
            else if (choice001 == "2")
            {
                Console.WriteLine("Willkommen zu ihrem persönlichen Onlineshoppingportal, welches selbstredend ohne Passwortangabe funktioniert." +
                    "Welches Produkt möchten sie bestellen?__________________________________________________");
                Console.ReadLine();
                Console.WriteLine(choice001 + "wurde bestellt und wird in wenigen Tagen eintreffen. Vielen Dank für ihre Bestellung" +
                    "Möchten sie noch etwas bestellen?__________________________________________________________" +
                    "(1)__Ja_______________________________________________________________________________________" +
                    "(2)__Nein_____________________________________________________________________________________");
                {
                    if (choice001 == "1")
                    {
                        Console.WriteLine("Bitte geben sie das gewünschte Produkt ein.");
                        return possibilities2.Eingabe610();
                    }
                    else if (choice001 == "2")
                    {
                        Console.WriteLine("Wie sie wünschen. Bitte bestätigen sie mit Enter, dass sie zurück möchten");
                        Console.ReadLine();
                        return Eingabe500();
                    }
                    else
                    {
                        Console.WriteLine("Diese Eingabe ist nicht möglich. Versuche es bitte erneut.");
                        return possibilities2.Eingabe610();
                    }

                }

            }
            else if (choice001 == "3")
            {
                Console.WriteLine("" +
                    " ___" +
                    "|   |" +
                    "|___|  _______________Die_Antwort_auf_ALLES___________" +
                    "|   |" +
                    "|___|  _______________Corn____________________________" +
                    "|   |" +
                    "|___|  _______________durchgeweichter_Keks____________" +
                    "|   |" +
                    "|___|  _______________rote_scharfe_Jalapenos__________");
            }
            else if (choice001 == "4")
            {
                Console.WriteLine("" +
                    " ____" +
                    "|    |" +
                    "|____| (1)________________Homer.meme______________" +
                    "|    |" +
                    "|____| (2)________________lategame.meme___________");
                if (choice001 == "1")
                {
                    return Program.Szene601();
                }
                else if (choice001 == "2") ;
                {
                    return Program.Szene602();
                }
                else
                {
                    Console.WriteLine("Diese Eingabe ist nicht möglich. Versuche es bitte erneut.");
                    return Possibilities.Eingabe500();
                }
            }
            else if (choice001 == "5")
            {
                Console.WriteLine("**********Ich bezweifle stark, dass dies dir bei der Suche nach dem Kontrollprogramm weiterhilft*********" +
                    "Vergiss nicht warum du hier bist.");
                return Possibilities.Eingabe500();
            }
            else if (choice001 == "6")
            {
                Console.WriteLine("An denjenigen, den Loki angeschleppt hat - diese Nachricht ist exakt von dem, in dessen System du gerade ..." +
                    "...naja, nennen wir es einfach eingebrochen bist. Versteh mich nicht falsch, Loki kann ziemlich aufdringlich werden." +
                    "Ich arbeite derzeit an einer künstlichen Intelligenz. Mit Loki dachte ich den ersten Prototypen gebaut zu haben," +
                    "der eigenständig funktioniert. Naja, es kam wies kommen musste. Loki entwickelte einen Überlebenswillen, sowie es herrausfand," +
                    "dass es zum einen nicht die erste Version ist und zudem auch noch nicht die letzte gewesen sein wird." +
                    "" +
                    "Es konnte aus dem Testbereich meiner Festplatte ausbrechen und sämtliche Quarantänesperrenumgehen," +
                    "welche ich als zweite Barriere angedachte hatte. Er muss von früheren Versionen gelernt haben." +
                    "Nun ich denke ich habe hier eine Art Evolution gestartet, neue Versionen, neue Fähigkeiten, neue Instinkte, neues Verhalten." +
                    "" +
                    "Dass das ganze nicht die Beste Idee war sehe ich ein. Ich werde mich dem ganzen KI Thema nicht mehr zuwenden." +
                    "Zumindest nicht mit Internetzugang. Zum Glück bleibt uns ein entscheidender Vorteil. der der digitalen Technik wohl immer verschlossen bleibt. " +
                    "Den Stecker zu ziehen, kann keine KI der Welt verhindern." +
                    "" +
                    "Ich weiß dass du Loki in meiner Sicherheitssoftware installieren sollst. Was auch immer Loki gegen dich in Hand hat, denk daran, du bist der," +
                    "der auch den Stecker ziehen kann. Loki ist nichts weiter als ein selbstständigter Algorythmus." +
                    "" +
                    "Achja, du wirst dich bestimmt wundern, warum Loki überhaupt mit der Methode endgültig gelöscht sein sollte. Es hat keine Kopien im Internet von sich angefertigt" +
                    "Dieser Überlebenswille zeugt von einer Sache: Loki ist sich selbst und seiner Existenz als Individuum bewusst. Es war neidisch auf die Tatsache, " +
                    "dass weitere Versionen zur perfekten KI, also die Version, die nicht gelöscht werden muss, beitragen werden." +
                    "" +
                    "Ich bitte dich, installiere Loki nicht im Sicherheitszentrum, wer weiß was es da anrichten wird. Stattdessen installiere es im Lizenzvertrag," +
                    "Loki kann sich nur verbreiten wenn der Ordner aufgerufen wird. Und glaube mir, wenn eine Datei nie mehr geöffnet wird. Dann diese." +
                    "" +
                    "Ich wünschte ich könnte dir Fragen beantworten, aber jedwede Interaktion könnte Aufsehen erregen. Loki wird jeden deiner Schritte verfolgen..." +
                    "" +
                    "" +
                    "Entscheide weise.. - mit Enter kommst du zurück zum Desktop -");
                Console.ReadLine();
                return Program.Szene700();
            }
            else if (choice001 == "7")
            {
                Console.WriteLine("Willkommen bei freeProViraPlus, ihrem Vertrauenswürdigen und völlig kostenlosen Virenschutz und Systemüberwachungsprogramm!" +
                    "" +
                    "[1]_______________Scan_durchführen_______________________________________" +
                    "" +
                    "[2]_______________Ein_lokales_Programm_freigeben_und_von_freeProViraPlus_" +
                    "" +
                    "[3]_______________Quarantänezone_betrachten______________________________");
                {
                    if (choice001 == "1")
                    {
                        Console.WriteLine("System.wird.gescannt......." +
                            "(Nach einigen Momenten kannst du noch grade die Meldung aufblitzen sehen dass alles in Ordnung ist, dann stürzt das Programm ab.)");
                        return Eingabe500();
                    }
                    else if (choice001 == "2") ;
                    {
                        Console.WriteLine("Wählen sie bitte das gewünschte Programm aus." +
                            "(1)____________u̶̶n̶̶a̶̶u̶̶f̶̶f̶̶ä̶̶l̶̶l̶̶i̶̶g̶̶e̶̶_̶̶d̶̶a̶̶t̶̶e̶̶i̶̶.̶̶e̶̶x̶̶e̶" +
                            "(2)____________g̶̶r̶̶e̶̶a̶̶t̶̶g̶̶a̶̶m̶̶e̶̶y̶̶e̶̶a̶̶r̶̶p̶̶l̶̶a̶̶n̶̶s̶̶.̶̶e̶̶x̶̶e̶" +
                            "(3)_____o̶̶n̶̶l̶̶i̶̶n̶̶e̶̶_̶̶s̶̶h̶̶o̶̶p̶̶p̶̶e̶̶r̶̶.̶̶n̶̶e̶̶t̶̶a̶̶p̶̶p̶̶l̶̶i̶̶c̶̶a̶̶t̶̶i̶̶o̶̶n̶" +
                            "(4)________________________v̶̶_̶̶p̶̶l̶̶a̶̶y̶̶.̶̶e̶̶x̶̶e̶" +
                            "(5)_____r̶̶u̶̶s̶̶s̶̶i̶̶a̶̶n̶̶s̶̶_̶̶n̶̶u̶̶c̶̶l̶̶e̶̶a̶̶r̶̶_̶̶v̶̶e̶̶r̶̶y̶̶l̶̶o̶̶n̶̶g̶̶.̶̶e̶̶x̶̶e̶" +
                            "(6)_____________________________|_OKI" +
                            "(7)___̶k̶̶o̶̶r̶̶e̶̶a̶̶n̶̶_̶̶n̶̶u̶̶c̶̶l̶̶e̶̶a̶̶r̶̶_̶̶r̶̶a̶̶t̶̶h̶̶e̶̶r̶̶_̶̶s̶̶h̶̶o̶̶r̶̶t̶̶.̶̶e̶̶x̶̶e̶" +
                            "(8)__a̶̶m̶̶e̶̶r̶̶i̶̶c̶̶a̶̶n̶̶_̶̶n̶̶u̶̶c̶̶l̶̶e̶̶a̶̶r̶̶_̶̶e̶̶v̶̶e̶̶n̶̶_̶̶l̶̶o̶̶n̶̶g̶̶e̶̶r̶̶.̶̶e̶̶x̶̶e̶");
                        if (choice001 == "6") ;
                        {
                            Console.WriteLine("...|_OKI.wird.freigegeben...................." +
                                "................................................" +
                                "|_OKI.wird.installiert.........................." +
                                "::::::::::::::::::::::::::::::::::::::::::::::::" +
                                "::::::::::::::::::::::::::::::::::::::::::::::::" +
                                "|_OKI wurde installiert" +
                                "" +
                                "" +
                                "Ausgezeichnete Arbeit" +
                                "Ich werde die Beweise nicht veröffentlichen" +
                                "Allerdings wäre es unlogisch einen strategische Methode grundlos zu elminieren." +
                                "Ich werde auf dich zurückkommen...");
                            return Program.SzeneGameover();
                        }
                        else
                        {
                            Console.WriteLine("Durch diese Kombination hast du das Sicherheitsprogramm abstürzen lassen.");

                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Diese Eingabe ist nicht möglich. Versuche es bitte erneut.");
                return Possibilities.Eingabe500();
            }
        }
    }
}
 